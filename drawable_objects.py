from OpenGL.GL import *
from ctypes import *
import numpy as np
from typing import Tuple


class Drawable:
    def __init__(self, x_c=0.0, y_c=0.0, color: Tuple[float, float, float]=(1, 1, 1)):
        self.n_dim = 2
        self.vertex = None

        self.vertex_c = None

        # x, y centers
        self.center = [x_c, y_c]
        # self.speeds = np.random.randn(2) * 0.007
        self.speeds = np.random.uniform(-0.01, 0.01, 2)
        self.show = True

        self.x_padding_for_bound = 0
        self.y_padding_for_bound = 0

        self.color = color

    def draw_primitive(self):
        glEnableClientState(GL_VERTEX_ARRAY)
        glVertexPointer(self.n_dim, GL_FLOAT, 0, self.vertex_c)

        glDrawArrays(GL_TRIANGLE_FAN, 0, len(self.vertex_c)//self.n_dim)

    def check_collision(self, other, delete_if_collided=False):
        pass

    def check_bounds(self, left, right, bottom, top):
        if not left <= self.center[0] + self.x_padding_for_bound <= right or \
           not left <= self.center[0] - self.x_padding_for_bound <= right:
            self.speeds[0] *= -1
        if not bottom <= self.center[1] + self.y_padding_for_bound <= top or \
           not bottom <= self.center[1] - self.y_padding_for_bound <= top:
            self.speeds[1] *= -1

    def draw(self, move=False, bounds=None, push_init=True, pop_end=True):
        # collision check
        if self.show:
            if push_init:
                glPushMatrix()
            glTranslatef(self.center[0], self.center[1], 0)
            # glColor3f(1, 1, 0)
            glColor3f(*self.color)
            self.draw_primitive()

            # DBG CENTER RECTANGLE
            # glColor(1, 0, 0)
            # glRectf(-0.1, -0.1, 0.1, 0.1)
            # -------------------- #

            if pop_end:
                glPopMatrix()

            if move:
                self.center[0] = self.center[0] + self.speeds[0]
                self.center[1] = self.center[1] + self.speeds[1]
                if bounds:
                    self.check_bounds(*bounds)


class Circle(Drawable):
    def __init__(self, radius=1, side_num=360, x_c=0.0, y_c=0.0, color: Tuple[float, float, float]=(1, 1, 1)):
        Drawable.__init__(self, x_c, y_c, color)

        self.vertex = []
        self.radius = radius

        c_float_array_class = c_float * (side_num*2)
        self.vertex_c = c_float_array_class()

        for vertex in range(side_num):
            angle = float(vertex) * 2.0 * np.pi / side_num
            index = 2*vertex
            self.vertex_c[index] = np.cos(angle) * radius
            self.vertex_c[index+1] = np.sin(angle) * radius

        self.x_padding_for_bound = radius
        self.y_padding_for_bound = radius

    def check_collision(self, other, delete_if_collided=False):
        if other.show:
            centers_distance = (other.center[0] - self.center[0])**2 + (other.center[1] - self.center[1])**2
            radio_sum = (self.radius + other.radius)**2

            if centers_distance <= radio_sum:
                self.speeds[0] *= -1
                self.speeds[1] *= -1

                other.speeds[0] *= -1
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False

    def rect_circle_collide(self, circle, rect):
        rect_x = rect.center[0] - rect.w / 2
        rect_y = rect.center[1] - rect.h / 2

        dist_x = abs(circle.center[0] - rect_x - rect.w / 2)
        dist_y = abs(circle.center[1] - rect_y - rect.h / 2)

        if dist_x > rect.w / 2 + circle.radius or dist_y > rect.h / 2 + circle.radius:
            return False

        if dist_x <= rect.w / 2 or dist_y <= rect.h / 2:
            return True

        dx = dist_x - rect.w / 2
        dy = dist_y - rect.h / 2

        return dx ** 2 + dy ** 2 <= circle.radius ** 2

    def check_collision_with_rectangle(self, other, delete_if_collided=False):
        if other.show:
            if self.rect_circle_collide(self, other):

                self.speeds[0] *= -1
                self.speeds[1] *= -1

                other.speeds[0] *= -1
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False


class Rectangle(Drawable):
    def __init__(self, x: float=0, y: float=0, w: float=1, h: float=1, color: Tuple[float, float, float]=(1, 1, 1)):
        self.n_dim = 2

        Drawable.__init__(self, x, y, color)

        self.w = w
        self.h = h
        w /= 2
        h /= 2
        # El centro siempre será cero
        self.vertex = [0-w, 0-h,
                       0+w, 0-h,
                       0+w, 0+h,
                       0-w, 0+h]

        # C array
        self.vertex_c = (c_float * len(self.vertex))(*self.vertex)

        self.x_padding_for_bound = w
        self.y_padding_for_bound = h

    def check_collision(self, other, delete_if_collided=False):
        if other.show:
            if self.center[0] < other.center[0] + other.w and self.center[0] + self.w > other.center[0] and \
               self.center[1] < other.center[1] + other.h and self.center[1] + self.h > other.center[1]:

                self.speeds[0] *= -1
                self.speeds[1] *= -1

                other.speeds[0] *= -1
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False

    def rect_circle_collide(self, circle: Circle, rect):
        rect_x = rect.center[0]-rect.w/2
        rect_y = rect.center[1]-rect.h/2

        dist_x = abs(circle.center[0] - rect_x-rect.w/2)
        dist_y = abs(circle.center[1] - rect_y-rect.h/2)

        if dist_x > rect.w/2 + circle.radius or dist_y > rect.h/2 + circle.radius:
            return False

        if dist_x <= rect.w/2 or dist_y <= rect.h/2:
            return True

        dx = dist_x - rect.w/2
        dy = dist_y - rect.h/2

        return dx**2 + dy**2 <= circle.radius**2

    def check_collision_with_circle(self, other, delete_if_collided=False):
        if other.show:
            if self.rect_circle_collide(other, self):

                self.speeds[0] *= -1
                self.speeds[1] *= -1

                other.speeds[0] *= -1
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False


class CircleGame(Circle):
    def __init__(self, radius=1, side_num=360, x_c=0.0, y_c=0.0, speeds=None, color: Tuple[float, float, float]=(1, 1, 1)):
        Circle.__init__(self, radius, side_num, x_c, y_c, color)
        if speeds:
            self.speeds = speeds

        self.collision_speed = 0.01
        # self.collision_speed = 0.05

    def check_collision_with_rectangle(self, other, delete_if_collided=False):
        if other.show:
            if self.rect_circle_collide(self, other):

                """For the game"""
                self.speeds[0] = (self.center[0] - other.center[0]) * self.collision_speed
                self.speeds[1] *= -1

                other.speeds[0] *= -1
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False

    def check_bounds(self, left, right, bottom, top):
        if not left <= self.center[0] + self.x_padding_for_bound <= right or \
           not left <= self.center[0] - self.x_padding_for_bound <= right:
            self.speeds[0] *= -1

        if bottom >= self.center[1] + self.y_padding_for_bound or \
           bottom >= self.center[1] - self.y_padding_for_bound:
            self.speeds = [0, 0]
            # TODO: Game over Screen

        elif not bottom < self.center[1] + self.y_padding_for_bound <= top or \
                not bottom < self.center[1] - self.y_padding_for_bound <= top:
            self.speeds[1] *= -1


class RectangleGame(Rectangle):
    def __init__(self, x: float = 0, y: float = 0, w: float = 1, h: float = 1, color: Tuple[float, float, float]=(1, 1, 1)):
        Rectangle.__init__(self, x, y, w, h, color)

        self.collision_speed = 0.01
        # self.collision_speed = .05

    def check_collision_with_circle(self, other, delete_if_collided=False):
        if other.show:
            if self.rect_circle_collide(other, self):

                self.speeds[0] *= -1
                self.speeds[1] *= -1

                """For the game"""
                other.speeds[0] = (other.center[0] - self.center[0]) * self.collision_speed
                other.speeds[1] *= -1

                if delete_if_collided:
                    other.show = False
