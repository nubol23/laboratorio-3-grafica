from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import gluOrtho2D
from drawable_objects import RectangleGame, CircleGame
from typing import Tuple


window = 0
width, height = 800, 830  # window size

mouse_x, mouse_y = 0, 0
bounds = (-4, 4, -4, 4)

start_game = False
rectangle_1, color_by_row, rectangles, circle_1 = None, None, None, None


def hex_to_rgb_scaled(color: str) -> Tuple[float, float, float]:
    r = int(color[1:3], 16)/255
    g = int(color[3:5], 16)/255
    b = int(color[5:], 16)/255

    return r, g, b


def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluOrtho2D(*bounds)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def draw():  # ondraw is called all the time
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clear the screen
    glLoadIdentity()  # reset position

    refresh2d(width, height)

    # User Input
    if not start_game:
        circle_1.center[0] = rectangle_1.center[0]

    rectangle_1.draw(move=False, bounds=bounds)
    circle_1.draw(move=start_game, bounds=bounds)

    rectangle_1.check_collision_with_circle(circle_1, delete_if_collided=False)

    for r in rectangles:
        circle_1.check_collision_with_rectangle(r, delete_if_collided=True)
        r.draw(move=False, bounds=bounds)

    glutSwapBuffers()


def control(mx, my):
    global mouse_x, mouse_y

    # Scaling to Screen
    mx = (mx * bounds[1]*2 / width) - bounds[1]
    my = -1 * ((my * bounds[3]*2 / height) - bounds[3])
    mouse_x, mouse_y = mx, my
    rectangle_1.center = [mx, rectangle_1.center[1]]


def start_onclick(button, state, x, y):
    global start_game
    if state == GLUT_DOWN and button == GLUT_LEFT_BUTTON:
        if not start_game:
            start_game = True

        if circle_1.speeds[0] == 0 and circle_1.speeds[1] == 0:
            init_variables()


def init_variables():
    global rectangle_1, color_by_row, rectangles, circle_1, start_game
    start_game = False

    rectangle_1 = RectangleGame(0, -3.70, 1.25, 0.15, hex_to_rgb_scaled('#13A0DB'))

    color_by_row = ['#C6C6C6', '#DF1F1E', '#19D71D', '#19D71D', '#C981CA', '#C9780D']

    rectangles = [RectangleGame(-3 + (0.55 * j), 2 + (0.25 * i), 0.5, 0.2, color)
                  for i, color in enumerate(map(hex_to_rgb_scaled, reversed(color_by_row)))
                  for j in range(12)]

    circle_1 = CircleGame(0.15, speeds=[0, 0.0065], x_c=0, y_c=-3.70 + 0.225)


if __name__ == '__main__':
    # rectangle_1 = RectangleGame(0, -3.70, 1.25, 0.15, hex_to_rgb_scaled('#13A0DB'))
    #
    # color_by_row = ['#C6C6C6', '#DF1F1E', '#19D71D', '#19D71D', '#C981CA', '#C9780D']
    #
    # rectangles = [RectangleGame(-3+(0.55*j), 2+(0.25*i), 0.5, 0.2, color)
    #               for i, color in enumerate(map(hex_to_rgb_scaled, reversed(color_by_row)))
    #               for j in range(12)]
    #
    # # circle_1 = CircleGame(0.15, speeds=[0.004, -0.0065])
    # circle_1 = CircleGame(0.15, speeds=[0, 0.0065], x_c=0, y_c=-3.70 + 0.225)

    init_variables()

    glutInit()  # initialize glut
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
    glutInitWindowSize(width, height)
    glutInitWindowPosition(0, 0)
    window = glutCreateWindow("Rect")
    glutDisplayFunc(draw)
    glutIdleFunc(draw)

    glutMouseFunc(start_onclick)

    glutPassiveMotionFunc(control)

    glutMainLoop()
